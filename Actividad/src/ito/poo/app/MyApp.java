package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.composicion;

public class MyApp {
	
	static void run() {
		composicion cp;
		
		cp= new composicion();
		
		System.out.println(cp);
		
		LocalDate fecha=LocalDate.now();
		
		System.out.println(new composicion("mienteme", 3.02f, "jessy", "pop", fecha));
		System.out.println(new composicion("Pineapple", 2.05f, "Karol G", "pop", fecha));
		System.out.println(new composicion("Nena Maldicion", 3.01f, "Paulo Londra", "electronica", fecha));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();

	}

}
